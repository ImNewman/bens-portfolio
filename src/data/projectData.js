export default [
    {
        projectTitle: "MARKUS SIEGEL</br> PORTFOLIO",
        projectImage: [
            require("../assets/images/markus-website/markus-website.jpg"),
            require("../assets/images/markus-website/markus-website-blur.jpg")
        ],
        role: "Web Designer & Developer",
        year: "Jan 2019",
        technology: ["WordPress", "Amplitude.js", "Fullpage.js"],
        description: "Markus Siegel is an aspiring film music composer who wanted a minimalist portfolio to showcase his work. The website features a custom music player built with Amplitude.js to stream his music.",
        link: "http://www.markussiegel.co.uk",
        logo: [],
        imagesDesktop: [
            require("../assets/images/markus-website/desktop-landing-resized.png"),
            require("../assets/images/markus-website/desktop-music-resized.png"),
            require("../assets/images/markus-website/desktop-video-resized.png")
        ],
        imagesMobile: [
            require("../assets/images/markus-website/mobile-landing.png"),
            require("../assets/images/markus-website/mobile-music.png"),
            require("../assets/images/markus-website/mobile-video.png")
        ]
    },
    {
        projectTitle: "TOM NEWMAN</br> PORTFOLIO",
        projectImage: [
            require("../assets/images/tom-website/tom-website-mood.jpg"),
            require("../assets/images/tom-website/tom-website-blur.jpg"),
        ],
        role: "Web Designer & Developer",
        year: "July 2017",
        technology: ["WordPress", "jQuery"],
        description: "Tom Newman is a film maker who produces a variety of content for companies within the eSports and MMA industry. The idea for his portfolio was a simple yet effective design, that puts his content at the forefront.",
        link: "http://www.tomnewman.eu",
        logo: [
            require("../assets/images/tom-website/main-logo2.png"),
        ],
        imagesDesktop: [
            require("../assets/images/tom-website/desktop-front.png"),
            require("../assets/images/tom-website/desktop-post.png"),
        ],
        imagesMobile: [
            require("../assets/images/tom-website/mobile-front.png"),
            require("../assets/images/tom-website/mobile-nav.png"),
            require("../assets/images/tom-website/mobile-post.png"),
        ]
    },
    {
        projectTitle: "JET2 &</br> JET2HOLIDAYS",
        projectImage: [
            require("../assets/images/jet2/jet2.jpg"),
            require("../assets/images/jet2/jet2-blur.jpg"),
        ],
        role: "Front End Developer",
        year: "Aug 2017 - Aug 2019",
        technology: ["Vue.js", "BEM", "Qubit", "Optimizely"],
        description: "At Jet2 I worked within a large team to deliver the new jet2holidays.com website. The website draws in millions of visits a month, so the emphasis was on building something that was scalable and reusable, as we continued to test and develop features post-launch.",
        link: "http://www.jet2holidays.com",
        logo: [
            require("../assets/images/jet2/jet2logo.png"),
            require("../assets/images/jet2/indulgentlogo.png"),
        ],
        imagesDesktop: [
            require("../assets/images/jet2/desktop-homepage.png"),
            require("../assets/images/jet2/desktop-search.png"),
            require("../assets/images/jet2/desktop-indulgent.png"),
            require("../assets/images/jet2/desktop-indulgentSearch.png"),
        ],
        imagesMobile: []
    }
]